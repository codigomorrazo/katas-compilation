# Kata compilation

This is a repo for practice purposes. I will include a compilation of different katas like FizzBuzz, Roman numbers and others.

## Requirements

These katas are generic and can be solved in a wide variety of languages, so there is no real requirement. 

Anyway I will add some docker configuration to avoid the necessity of installing a server or other dependencies on the computers. 
That way we will just need to run the chosen docker instance and start coding.

    · NOTE: I can't add all the docker needed for all the existing languages. So, I will add those instances used to solve the katas in each moment.

## Common kata rules for TDD
    1. No code production code will be writen until there is a unit test who requires it.
    2. We must write just one unit test at a time. It must be enough to fail. Compilation errors count as fails.
    3. We won't write more production code than the needed to pass the current failing test.

## RED-GREEN-REFACTOR CICLE
We will follow three steps cycle to solve all the proposed kata
    1. Write one test that fail for a single reason.
    2. Write just the needed production code to pass that test.
    3. With all the tests in green, do refactor to improve the existing code. Without adding any new functionality.
## KATAS
    · There isn't any specific order to solve the katas
 
| Kata             | Description                                                                                                                   |
|------------------|-------------------------------------------------------------------------------------------------------------------------------|
| 1. FizzBuzz      | Replace divisible by three numbers with Fizz, divisible by five with Buzz and those divisible by three and five with FizzBuzz |
| 2. Greetings     | Print a required greeting with uncertain requirements                                                                         |
| 3. Prime factors | Find the prime factors of a given number                                                                                      |
| 4. Roman numbers | Convert an integer in its roman form                                                                                          |
| 5. Tic tac toe   | Traditional board game where we must score three symbols in a row                                                             |
| 6. NIF           | Validate a string as a valid spanish identification document                                                                  |
| 7. Leap years    | Return true or false if a provided year is leap or it isn't                                                                   |

    NOTE: you can find more katas at https://codingdojo.org/kata/