# NIF 
This kata must validate a provided string and return if it is a valid NIF in Spain.
A valid NIF is composed by 8 numbers followed by a final letter as control char.
This control char is calculated with the remain of dividing the numeric part by 23. We will use that number to obtain the letter in the next table:

| Number | Letter |
|--------|--------|
| 0      | T      |
| 1      | R      |
| 2      | W      |
| 3      | A      |
| 4      | G      |
| 5      | M      |
| 6      | Y      |
| 7      | F      |
| 8      | P      |
| 9      | D      |
| 10     | X      |
| 11     | B      |
| 12     | N      |
| 13     | J      |
| 14     | Z      |
| 15     | S      |
| 16     | Q      |
| 17     | V      |
| 18     | H      |
| 19     | L      |
| 20     | C      |
| 21     | K      |
| 22     | E      |

There is a special case with this. The NIE, that's the identification to foreigners. 
In this case the first character will be a letter X, Y or Z. To calculate the control character, those chars will be replaced by 0, 1 and 2, respectively.

What we will learn with this kata is to discard some results before to try to find the happy paths.

    This kata was created by Fran Iglesias. You can find it in his book `Aprende test driven development`

## Hints
 * We will start discarding invalid cases. The result of this is that we will have a more resilient code.
 * The solution of the kata will be delayed until the next test. To make a test pass, we will introduce a strict implementation to pass just that test. The previous ones must keep passing.

