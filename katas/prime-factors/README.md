# Prime factors
The objective of this kata is to notice that when the tests get more specific the code becomes more general.

We will create a class with a generate method or simply a function. This method must return a list with the prime numbers of an integer.
The result can be output as we desire, an array, list, collection, etc.

Output examples:
```
primefactors(1) = []
primefactors(2) = [2]
primefactors(3) = [3]
primefactors(4) = [2,2]
primefactors(6) = [2,3]
primefactors(8) = [2,2,2]
...
```
As you may notice, we will not take into account the number 1 as long as any integer will be divisible by it.

## Guidelines
We can divide the numbers in three categories

    · Numbers without prime factors. The only one is 1.
    · Prime numbers like 2, 3 or 5
    · Those that are the result of several prime numbers, like 4, 6, 8 or 9