# Greetings

This is a kata simple to explain but with an interesting development. 
The objective of this kata is to create a pure function `greet`. No other methods or functions are allowed. 
All the code must be inside the greet function.
This function must return a string with a greeting. As a parameter will receive the name of the person.

The most important rule of this kata is to get the requirements one at a time. 
That way we will not know the next step, so we cannot go beyond the actual requirement.
This way we must evolve our algorithm with each new information.

    Try to not show the next requirement until you get the previous one solved and all the tests passing.
1. Requirement: Simple response. Example: input `Bob`, output `Hello, Bob`
2. Requirement:
>!If there is no name in the input, return a generic response. Example: input `null`, output: `Hello, my friend`
3. Requirement: 
>!If some one shouts his name, answer screaming. Example: input `JERRY`, output: `HELLO, JERRY!`
4. Requirement:
>!Handle two names. Just two names, don't go further. Example: input `"Jill","Jane"`, output: `Hello, Jill and Jane`
5. Requirement:
>!Handle any amount of names, with colon Oxford style. Example: input `"Amy", "Brian", "Charlotte"`, 
> output `Hello, Amy, Brian and Charlotte`
6. Requirement:
>!Allow to mix names in lowercase and upper case, but handle the answer separatedly. Example: input `"Amy", "BRIAN", "Charlotte"`,
> output `Hello, Amy and Charlotte. AND HELLO BRIAN!`
7. Requirement: 
>!If a name contains a colon, separate it. Example: input `"Bob", "Charlie, Dianne"`, output: `Hello, Bob, Charlie and Dianne.
8. Requirement:
>!Allow to escape de colons. Example: input `"Bob", "Charlie\,Dianne"`, output `Hello, Bob and Charlie, Dianne`


    The most important part of the kata is to resolve it with just requirement at a time. So try to do not jump ahead.
    Remenber, do not create other functions, classes or whatever.