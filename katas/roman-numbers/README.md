# ROMAN NUMBERS

This is a great kata. We need to create a class, or function that will receive an integer and must return its equivalent un roman nomenclature.

As a recordatory, here goes the Roman number and their equivalent.

|Roman number|Arabic number|
|------------|-------------|
| I | 1 |
| V | 5 |
| X | 10 |
| L | 50 |
| C | 100 |
| D | 500 |
| M | 1000 |

This way you should be able to convert some numbers to its roman form.
Here are some examples:

|Arabic | Roman |
|-------|-------|
| I | 1 |
| II | 2 |
| III | 3 |
| IV | 4 |
| 5 | V |
| 6 | VI |
| 7 | VII |
| 8 | VIII |
| 9 | XIX |
| 10 | X |
| 11 | XI |
| 12 | XII |
| 15 | XV |
| 18 | XVIII |
| 19 | XIX |
| 20 | XX |
| 39 | XXXIX |
| 49 | XLIX |
| 50 | L |
| 79 | LXXIX |
| 99 | XCIX |
| 499 | CDXCIX |
| 999 | CMXCIX |
| 2021 | MMXXI |

I think that those examples are enough to start the kata. Do as much tests as you need.