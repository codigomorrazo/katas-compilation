# Leap years

In this kata we must create an object or function, that must return if the given year is leap or it isn't.

## Specifications
    · Any year divisible by 400 are a leap year
    · All years divisible by 100 but not by 400 are not leap years
    · All years divisible by 4 but not for 100 are leap years
    · Any year not divisible by 4 are not leap years

Examples:
```
leapYear(2000) = true
leapYear(1700) = false
leapYear(2008) = true
leapYear(2017) = false
```
