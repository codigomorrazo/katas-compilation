# FizzBuzz
In this kata we must create a class, function, etc, that must replace the given number if needed.
But each number divisible by 3 must be replaced with Fizz, those divisible by 5 replaced with Buzz and when a number is divisible by both factors will be replaced with FizzBuzz. Any other number will be returned normally.

We will do a 1 to 100 printing loop with the return of the function. An expected output as an example from 1 to 15 would be:
```
1
2
Fizz
4
Buzz
Fizz
7
8
Fizz
Buzz
11
Fizz
13
14
FizzBuzz
```

## Requirements
Remember to follow the general rules on the principal README.md of the project.

Try to solve it as if the Product Owner will give as the instructions one by one. That way we can't get ahead of what we are doing.

    1. If the number is divisible by 3 must return Fizz
    2. If the number is divisible by 5 must return Buzz
    3. If the number is divisible for 3 and 5 must return FizzBuzz