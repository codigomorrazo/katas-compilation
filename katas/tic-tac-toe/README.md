# Tic tac toe
## Description
The board game is a grid of three column/row, and we will use the symbols 'x' and 'o'.
The game will be finished when any player sets a three equal symbol line. It could be a horizontal, vertical or diagonal line.
The first player will be 'x' and each player will set one symbol per turn.
We can put the symbol in any free cell.

If all the cells are filled, but there is no winner, the game will end in draw.

## Rules for this kata
    . Just one level identation in each method or function
    . The use of 'else' is forbidden
    . Every primitive type must be encapsuled in objects
    . Do not chain calls, just a call for line
    . Do no use name abreviation 
    . Keep classes below 50 lines and methods or function below 5 lines
    . Do not use more than two members on a class
    . Neither use getters or setters, nor access private properties 